Boomtown
=========

Boomtown is an steam punk, wild west, alternate history, pen-and-paper
RPG about Cowboys and [Native] Americans in space (they get there with
the help of French scientist Jules Verne).

Boomtown is designed to be:

* Quick to start: Character creation is fun and fast.
* Easy to learn: The basic rules use a very simple dice mechanic.
* Flexible: Can use any variety of die. No die? Flip a coin.
* Fun: The rules are basically common sense.

It's a non-intimidating way to introduce new players to role-playing,
and it's a fun quick game to run in a limited amount of time.


How do I play?
----------------

Download a rendered copy of this game from the ''dist'' directory and have fun.


It's too simple.
-----------------

Never fear. If you love the Boomtown setting but want advanced RPG
rules, it's 99% compatible with the [Open
d6](http://opend6.wikidot.com/d6-adventure) system. You probably will
want to make some minor adjustments to the weapons and vehicles so
that they're old-timey and steampunky, but that's just a matter of
"skinning" and can be done on-the-fly.


How do I make this game better?
---------------------------------

This game is open source. You can improve it, if you have cool ideas
or see glaring omissions that you think hold it back from being easy
and fun. Just clone this repository, make changes, and send me
patches. Or fork the repository and request a merge.

The game is licensed under a Creative Commons license, meaning you can
share it, copy it, modify it, and even sell it. However, you must
share your changes under the same license.

See the LICENSE file for details.